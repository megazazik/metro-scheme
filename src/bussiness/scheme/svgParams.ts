import {
	ITEM_TYPE_SECTION,
	ITEM_TYPE_STATION
} from '../map/interface';

export interface ISvgParamsScheme {
	type: 'SvgParamsScheme';
	mapId: string;
	items: {[id: string]: SvgParamsItem};
	attributes?: {[name: string]: any};
	lines: {[id: string]: ISvgParamsLine};
}

export type  SvgParamsItem = ISvgParamsItemStation | ISvgParamsItemSection;

export interface ISvgParamsLine {
	id: string;
	color: string;
}

export interface ISvgParamsItemBase {
	mapItemId: string;
	lineId: string;
}

export interface ISvgParamsItemStation extends ISvgParamsItemBase {
	type: typeof ITEM_TYPE_STATION;
	x: number;
	y: number;
}

export interface ISvgParamsItemSection extends ISvgParamsItemBase {
	type: typeof ITEM_TYPE_SECTION;
	path: string;
}

export { ITEM_TYPE_SECTION, ITEM_TYPE_STATION };