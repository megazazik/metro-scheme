import { ISvgParamsScheme } from './svgParams';
export * from './svgParams';
export type Scheme = ISvgParamsScheme;