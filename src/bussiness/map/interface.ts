export interface IMap {
	id: string;
	items: {[id: string]: MapItem[]};
}

export type MapItem = IStation | IChange | ISection;

export const ITEM_TYPE_STATION = 'station';
export const ITEM_TYPE_CHANGE = 'change';
export const ITEM_TYPE_SECTION = 'section';

interface IMapItemBase {
	id: string;
}

export interface IStation extends IMapItemBase {
	type: typeof ITEM_TYPE_STATION;
}

export interface IChange extends IMapItemBase {
	type: typeof ITEM_TYPE_CHANGE;
}

export interface ISection extends IMapItemBase {
	type: typeof ITEM_TYPE_SECTION;
}