import * as React from 'react';
import { renderToString } from 'react-dom/server';
import PageView from 'components/buildTest';

export default (): string => {
	const currentLanguage = 'rus';
	const html = renderToString(React.createElement(PageView, {locale: currentLanguage}));
	
	return `
<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Typescript application template</title>
		<link rel="stylesheet" href="/static/index.css" />
	</head>
	<body>
		<div id="content">${html}</div>
		<script src="/static/index.rus.js"></script>
		<script src="/static/index.eng.js"></script>
		<script src="/static/index.js"></script>
	</body>
</html>`;
};