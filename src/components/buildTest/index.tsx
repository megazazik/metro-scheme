import * as React from 'react';
import styles from './styles.less';
import img from './img.png';
let labels = {};
const getLabels = (locale: string) => {
	if (!labels[locale]) {
		labels[locale] = require(`./labels/${locale}.json`);
	}
	return labels[locale];
};

export interface IProps {
	locale: string;
}

export default function PageView(props: IProps) {
	return (
		<div>
			<h1>{getLabels(props.locale).title}</h1>
			<div className={styles.text}>{getLabels(props.locale).text}</div>
			<div><img src={img}/></div>
		</div>
	);
}