// tslint:disable: jsx-no-lambda
import * as React from 'react';
import { storiesOf } from '@storybook/react';
// import { withKnobs, array, text, number, boolean } from '@storybook/addon-knobs';
// import { withInfo } from '@storybook/addon-info';
import Page from './';

storiesOf('Page', module)
	// .addDecorator(withKnobs)
	// .add('Default', withInfo('')(() => (
	.add('Default', () => (
		<Page
			locale='rus'
		/>
	))
	// .add('Eng', withInfo('')(() => (
	.add('Eng', () => (
		<Page
			locale='eng'
		/>
	));