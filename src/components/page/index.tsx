import * as React from 'react';
import { Scheme } from 'bussiness/scheme/interface';
import SchemeComponent from 'components/scheme';

const pageParams: {scheme: Scheme} = {
	scheme: {
		type: 'SvgParamsScheme',
		mapId: "1",
		lines: {},
		items: {}
	}
}

export default function PageView() {
	return <SchemeComponent {...pageParams} />;
}