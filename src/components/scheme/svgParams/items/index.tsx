import * as React from 'react';
import {
	SvgParamsItem,
	ITEM_TYPE_SECTION,
	ITEM_TYPE_STATION
} from 'bussiness/scheme/interface';
import { ICommonProps } from './interface';
import Station from './station';
import Section from './section';

export interface IProps extends ICommonProps {
	item: SvgParamsItem;
}

export default function Scheme (props: IProps) {
	const {item, ...restProps} = props;
	switch (item.type) {
		case ITEM_TYPE_STATION: return <Station {...restProps} item={item} />;
		case ITEM_TYPE_SECTION: return <Section {...restProps} item={item} />;
		default:  {
			if (console && console.warn) {
				console.warn(`No component was found for scheme element wih type "${(item as any).type}".`);
			}
			return null;
		}
	}
}