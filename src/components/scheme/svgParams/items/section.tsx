import * as React from 'react';
import { ISvgParamsItemSection } from 'bussiness/scheme/interface';
import { ICommonProps } from './interface';

export interface IProps extends ICommonProps {
	item: ISvgParamsItemSection;
}

export default class SvgParamsItemSection extends React.PureComponent<IProps> {
	render() {
		const { styles, selected, item, color } = this.props;
		return (
			<path
				stroke={color}
				d={item.path}
				className={`${styles.section} ${selected ? styles.selected : ''}`}
			/>
		);
	}
}