import * as React from 'react';
import { ISvgParamsItemStation } from 'bussiness/scheme/interface';
import { ICommonProps } from './interface';

export interface IProps extends ICommonProps {
	item: ISvgParamsItemStation;
}

interface IState {
	hovered: boolean;
}

export default class SvgParamsItemStation extends React.PureComponent<IProps, IState> {
	public state: IState = {
		hovered: false
	}

	private _setHovered = () => {
		this.setState({hovered: true});
	}

	private _setUnhovered = () => {
		this.setState({hovered: false});
	}

	render() {
		const { item, styles, color } = this.props;
		const big = this.props.selected || this.state.hovered;

		return (
			<g>
				{this.props.selected && (
					<circle
						cx={item.x}
						cy={item.y}
						r={big ? 25 : 23}
						fill={color}
						className={`${styles.stationBorder} ${this.state.hovered ? styles.hovered: ''}`}
					/>
				)}
				<circle
					cx={item.x}
					cy={item.y}
					r={big ? 23 : 20}
					fill={color}
					onMouseEnter={this._setHovered}
					onMouseLeave={this._setUnhovered}
					className={[
						styles.station,
						this.props.selected ? styles.selected : '',
						this.state.hovered ? styles.hovered : ''
					].join(' ')}
				/>
			</g>
		);
	}
}