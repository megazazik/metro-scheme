export interface ICommonProps {
	/** @todo make it mandatory */
	selected?: boolean;
	styles: {[name: string]: string};
	color: string;
}