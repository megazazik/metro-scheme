import * as React from 'react';
import { ISvgParamsScheme } from 'bussiness/scheme/interface';
import SvgParamsItem from './items';
import * as styles from './styles.less';

export interface IProps {
	scheme: ISvgParamsScheme;
}

export default function SvgParamsScheme (props: IProps) {
	const {lines, items} = props.scheme;
	return (
		<svg {...props.scheme.attributes}>
			{Object.keys(items).map((itemId, index) => (
				<SvgParamsItem
					item={items[itemId]}
					key={index}
					styles={styles}
					color={lines[items[itemId].lineId].color}
				/>
			))}
		</svg>
	);
}