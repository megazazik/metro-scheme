import * as React from 'react';
import { ISvgParamsScheme } from 'bussiness/scheme/interface';
import SvgParamsScheme, { IProps } from '../index';

export const previewProps: IProps = {
	scheme: {
		type: 'SvgParamsScheme',
		mapId: "1",
		attributes: {viewBox: '0 0 1000 1000', style: {width: '100%', height: '100%'}},
		lines: {
			1: {
				id: '1',
				color: 'brown'
			},
			2: {
				id: '2',
				color: 'rgb(96, 255, 47)'
			}
		},
		items: {
			1: {
				mapItemId: '1',
				lineId: '1',
				type: 'section',
				path: "M 300 500 A 200 200 0 0 1 500 300"
			},
			2: {
				mapItemId: '2',
				lineId: '1',
				type: 'section',
				path: "M 500 300 A 200 200 0 0 1 700 500"
			},
			3: {
				mapItemId: '3',
				lineId: '1',
				type: 'section',
				path: "M 700 500 A 200 200 0 0 1 500 700"
			},
			4: {
				mapItemId: '4',
				lineId: '1',
				type: 'section',
				path: "M 500 700 A 200 200 0 0 1 300 500"
			},
			6: {
				mapItemId: '6',
				lineId: '1',
				type: 'station',
				x: 700,
				y: 500
			},
			7: {
				mapItemId: '7',
				lineId: '1',
				type: 'station',
				x: 500,
				y: 700
			},
			8: {
				mapItemId: '8',
				lineId: '1',
				type: 'station',
				x: 300,
				y: 500
			},
			9: {
				mapItemId: '9',
				lineId: '1',
				type: 'station',
				x: 500,
				y: 300
			},
			10: {
				mapItemId: "10",
				lineId: "2",
				type: "station",
				x: 535,
				y: 303
			},
			11: {
				mapItemId: "11",
				lineId: "2",
				type: "station",
				x: 465,
				y: 697
			},
			13: {
				mapItemId: "13",
				lineId: "2",
				type: "station",
				x: 535,
				y: 203
			},
			14: {
				mapItemId: "14",
				lineId: "2",
				type: "station",
				x: 465,
				y: 797
			},
			12: {
				mapItemId: '12',
				lineId: '2',
				type: 'section',
				path: "M 535 303 L 465 697"
			},
			16: {
				mapItemId: '16',
				lineId: '2',
				type: 'section',
				path: "M 535 303 L 535 203"
			},
			17: {
				mapItemId: '17',
				lineId: '2',
				type: 'section',
				path: "M 465 697 L 465 797"
			}
		}
	}
}

export default function PreviewScheme(props: IProps) {
	return (
		<div style={{width: '100%', height: '300px'}}>
			<SvgParamsScheme {...props}/>
		</div>
	)
}