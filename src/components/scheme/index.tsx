import * as React from 'react';
import { Scheme } from 'bussiness/scheme/interface';
import SvgParamsScheme from './svgParams';

export interface IProps {
	scheme: Scheme;
}

export default function Scheme (props: IProps) {
	switch (props.scheme.type) {
		case 'SvgParamsScheme': return <SvgParamsScheme {...props} />
		default:  throw new Error(`No component was found for scheme wih type "${props.scheme.type}".`)
	}
}