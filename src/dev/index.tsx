import * as ReactDom from 'react-dom';
import * as React from 'react';
import PageView from './hotPage';

ReactDom.hydrate(
	React.createElement(PageView, {locale: 'rus'}),
	document.getElementById('content')
);
