'Use strict';
const { createClientEntry } = require('@megazazik/build');
const common = require('./common');

module.exports = createClientEntry({
	...common,
	entry: {index: './src/dev/index'},
	emitFiles: true,
	tsconfig: 'tsconfig.dev.json',
});