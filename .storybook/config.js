import { configure } from '@storybook/react';

function loadStories() {
  require('@megazazik/build/stub');
}

configure(loadStories, module);