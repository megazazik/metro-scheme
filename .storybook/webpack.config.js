const path = require("path");
const { getStorybookConfig } = require('@megazazik/build');

module.exports = getStorybookConfig({contextPath: path.resolve(__dirname, '../')});
